#!/usr/bin/env bash


dotnet run \
   --project Bible.Classification.WebApi/Bible.Classification.WebApi.csproj \
   --configuration Release \
   --verbosity m