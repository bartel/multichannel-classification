﻿using System.Collections.Generic;
using Bible.Classifiers.Binary;
using Bible.Classifiers.Dtos;
using Bible.Classifiers.MultiChannel;
using Microsoft.AspNetCore.Mvc;

namespace Bible.Classification.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MultiClassController : ControllerBase
    {
        private readonly IBinaryModelPredictionService  _toxicityPredictionService;
        private readonly IBinaryModelTrainerService     _toxicityModelTrainerService;
        private readonly IMultiChannelTrainerService    _multichannelTrainerService;
        private readonly IMultiChannelPredictionService _multichannelPredictionService;

        public MultiClassController(IBinaryModelPredictionService  toxicityPredictionService,
                                    IBinaryModelTrainerService     toxicityModelTrainerService,
                                    IMultiChannelTrainerService    multichannelTrainerService,
                                    IMultiChannelPredictionService multichannelPredictionService)
        {
            _toxicityPredictionService     = toxicityPredictionService;
            _toxicityModelTrainerService   = toxicityModelTrainerService;
            _multichannelTrainerService    = multichannelTrainerService;
            _multichannelPredictionService = multichannelPredictionService;
        }

        // GET api/values
        [HttpGet("predict/")]
        public IActionResult Predict(string phrase)
        {
            var toxicPrediction = _toxicityPredictionService.Predict(phrase);
            if (toxicPrediction.Probability > 0.7)
            {
                return BadRequest("Your message seems to be very toxic, sorry");
            }
            return Ok(_multichannelPredictionService.Predict(phrase));
        }

        // POST api/multiclass
        [HttpPost]
        public void Train()
        {
            _toxicityModelTrainerService.BuildTrainEvaluateAndSaveModel();
            _multichannelTrainerService.BuildTrainEvaluateAndSaveModel();
        }
    }
}