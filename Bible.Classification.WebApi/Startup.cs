﻿using Bible.Classifiers;
using Bible.Classifiers.Binary;
using Bible.Classifiers.MultiChannel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static Bible.Classifiers.MultiChannel.MultiChannelTrainerStrategy;

namespace Bible.Classification.WebApi
{
    public class Startup
    {
        private const string BaseModelPath = "./Data";
        private const string SentimentalModelFileName = "SentimentalModel";
        private const string BibleModelFileName = "SentimentalMultiClassModel";
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            services.AddSingleton<IMultiChannelPredictionService>(
                new MultiChannelPredictionService($"{BaseModelPath}/{BibleModelFileName}.zip"));
            
            services.AddSingleton<IBinaryModelPredictionService>(
                new BinaryModelPredictionService(baseModelPath: BaseModelPath, 
                                                 modelFileName: SentimentalModelFileName));
            services.AddSingleton<IBinaryModelTrainerService>(
                new BinaryModelTrainerService(trainDataPath: "./Data/train.tsv",
                                              testDataPath: "./Data/test.tsv", 
                                              baseModelPath: BaseModelPath, 
                                              modelFileName: SentimentalModelFileName));
            services.AddSingleton<IMultiChannelTrainerService>(
                new MultiChannelTrainerService(trainDataPath: "./Data/sentiments.tsv",
                                               baseModelPath: BaseModelPath,
                                               modelFileName: BibleModelFileName,
                                               trainerStrategy: OvaAveragedPerceptronTrainer));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}