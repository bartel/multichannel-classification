### multichannel-classification
#### Dependencies
 - [.NET Core SDK](https://dotnet.microsoft.com/download)

#### How to run

In order to run the project you need to execute followed instructions:
```bash
dotnet run --project Bible.Classification.WebApi/Bible.Classification.WebApi.csproj --configuration Release --verbosity m
```

#### How to test

After running the project you can use following requests to test the api
```
################# Toxic Test

GET https://localhost:5001/api/multiclass/predict?phrase=You, go killll yourself

################# Sadness

GET https://localhost:5001/api/multiclass/predict?phrase=I'm just tired and so alone

################# Anna Frank letter, extract 1

GET https://localhost:5001/api/multiclass/predict?phrase=I'm finally getting optimistic. Now, at last, things are going well! They really are! Great news!

################# Anna Frank letter, extract 2

GET https://localhost:5001/api/multiclass/predict?phrase=An assassination attempt has been made on Hitler's life, and for once not by Jewish Communists or English capitalists, but by a German general who's not only a count, but young as well. The Fuhrer owes his life to "Divine Providence": he escaped, unfortunately, with only a few minor burns and scratches. A number of the officers and generals who were nearby were killed or wounded. The head of the conspiracy has been shot.

################# St. Therese of Lisieux letter, extract

GET https://localhost:5001/api/multiclass/predict?phrase=Already God sees us in glory, and rejoiced in our everlasting bliss. How much good I derive from this thought!

################# Gospel of the Lord, extract

GET https://localhost:5001/api/multiclass/predict?phrase=Love the Lord your God with all your heart and with all your soul and with all your mind and with all your strength. The second is this: ‘Love your neighbor as yourself.

################# Some text

GET https://localhost:5001/api/multiclass/predict?phrase=I really don’t hate you. I’m just disappointed that you wound up turning into everything you said that you would never be.
```

