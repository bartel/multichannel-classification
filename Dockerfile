FROM mcr.microsoft.com/dotnet/core/sdk:2.2

COPY . /app
WORKDIR /app
EXPOSE 5000

RUN dotnet run \
    --project Bible.Classification.WebApi/Bible.Classification.WebApi.csproj \
    --configuration Release \
    --urls "http://0.0.0.0:5000" \
    --verbosity m
