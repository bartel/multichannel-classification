using System.IO;

namespace Bible.Classifiers
{
    public static class StringExtensions
    {
        private static readonly FileInfo DataRoot = new FileInfo(typeof(StringExtensions).Assembly.Location);

        public static string ToAbsolutePath(this string me)
        {
            var assemblyFolderPath = DataRoot.Directory?.FullName;
            var fullPath = Path.Combine(assemblyFolderPath , me);
            return fullPath;
        }

        public static FileStream PathToNewFileStream(this string me)
        {
            return new FileStream(me, 
                                  FileMode.Create,
                                  FileAccess.Write,
                                  FileShare.Write);
        }
    }
}