using Microsoft.ML.Data;

namespace Bible.Classifiers.Dtos
{
    public class MultichannelPrediction
    {
        [ColumnName("PredictedLabel")]
        public string Sentiment;

        public float[] Score;
    }
}