using System.Numerics;
using Microsoft.ML.Data;

namespace Bible.Classifiers.Dtos
{
    public class RawTwitterSentimentEntity
    {
        // tweet_id	sentiment	author	content
        [LoadColumn(0)]
        public int TweetId { get; set; }
        [LoadColumn(1)]
        public string Sentiment { get; set; }
        [LoadColumn(2)]
        public string Author { get; set; }
        [LoadColumn(3)]
        public string Content { get; set; }
    }
}