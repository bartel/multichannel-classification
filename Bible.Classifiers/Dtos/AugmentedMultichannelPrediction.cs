namespace Bible.Classifiers.Dtos
{
    public class AugmentedMultichannelPrediction
    {
        public string PredictedSentiment  { get; }
        public float  Score               { get; }
        public int    OriginalSchemaIndex { get; }

        public AugmentedMultichannelPrediction(string predictedSentiment,
                                               float  score,
                                               int    originalSchemaIndex)
        {
            OriginalSchemaIndex = originalSchemaIndex;
            PredictedSentiment  = predictedSentiment;
            Score               = score;
        }
    }
}