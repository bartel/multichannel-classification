using Microsoft.ML.Data;

namespace Bible.Classifiers.Dtos
{
    public class RawVerseEntry
    {
        [LoadColumn(0)] public string Type { get; set; }
        [LoadColumn(1)] public string Text { get; set; }
    }
}