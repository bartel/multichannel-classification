using Microsoft.ML.Data;

namespace Bible.Classifiers.Dtos
{
    public class RawLabeledEntry
    {
        [LoadColumn(0)]
        public bool Label { get; set; }
        [LoadColumn(1)]
        public string Text { get; set; }
    }
}