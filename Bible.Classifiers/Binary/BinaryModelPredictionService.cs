using System;
using System.IO;
using Bible.Classifiers.Dtos;
using Microsoft.ML;

namespace Bible.Classifiers.Binary
{
    public interface IBinaryModelPredictionService
    {
        Prediction Predict(string text);
    }

    public class BinaryModelPredictionService : IBinaryModelPredictionService
    {
        private readonly string _modelPath;
        private readonly MLContext _mlContext;
        private readonly Lazy<PredictionEngine<RawLabeledEntry, Prediction>> _predictionEngine;

        public BinaryModelPredictionService(string baseModelPath, string modelFileName = "BinaryClassModel")
        {
            _modelPath = $"{baseModelPath.ToAbsolutePath()}/{modelFileName}.zip";
            _mlContext = new MLContext(seed: 1);
            _predictionEngine = new Lazy<PredictionEngine<RawLabeledEntry, Prediction>>(GetPredictionEngine);
            
        }

        public Prediction Predict(string text)
        {
            var sampleStatement = new RawLabeledEntry { Text = text };
            var predictionEngine = _predictionEngine.Value;
            //Score
            var prediction = predictionEngine.Predict(sampleStatement);

            Console.WriteLine($"=============== Single Prediction  ===============");
            Console.WriteLine($"Text: {sampleStatement.Text} | Prediction: {(Convert.ToBoolean(prediction.PredictedLabel) ? "Negative" : "Nice")} sentiment | Probability: {prediction.Probability} ");
            Console.WriteLine($"==================================================");
            return prediction;
        }

        private PredictionEngine<RawLabeledEntry, Prediction> GetPredictionEngine()
        {
            var trainedModel = LoadTrainedModel();

            // Create prediction engine related to the loaded trained model
            var predictionEngine = trainedModel.CreatePredictionEngine<RawLabeledEntry, Prediction>(_mlContext);
            return predictionEngine;
        }

        private ITransformer LoadTrainedModel()
        {
            using (var stream = new FileStream(_modelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return _mlContext.Model.Load(stream);
            }

        }
    }
}