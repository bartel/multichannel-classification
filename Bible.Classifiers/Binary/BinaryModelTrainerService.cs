﻿using System;
using Bible.Classifiers.Dtos;
using Bible.Common;
using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers.FastTree;

namespace Bible.Classifiers.Binary
{
    public interface IBinaryModelTrainerService
    {
        ITransformer BuildTrainEvaluateAndSaveModel();
        ITransformer BuildTrainEvaluateAndSaveModel(MLContext mlContext);
    }

    public class BinaryModelTrainerService : IBinaryModelTrainerService
    {
        private static readonly string BaseDatasetsPath = @"./Data".ToAbsolutePath();
        private readonly string _trainDataPath;
        private readonly string _testDataPath;
        private readonly string _modelPath;
        private readonly MLContext _mlContext;

        public BinaryModelTrainerService(string trainDataPath, string testDataPath, string baseModelPath, string modelFileName = "BinaryClassModel")
        {
            _trainDataPath = trainDataPath.ToAbsolutePath();
            _testDataPath = testDataPath.ToAbsolutePath();
            _modelPath = $"{baseModelPath.ToAbsolutePath()}/{modelFileName}.zip";
            _mlContext = new MLContext(seed: 1);
        }

        public ITransformer BuildTrainEvaluateAndSaveModel()
        {
            return BuildTrainEvaluateAndSaveModel(_mlContext);
        }
        
        public ITransformer BuildTrainEvaluateAndSaveModel(MLContext mlContext)
        {
            // STEP 1: Bible.Common data loading configuration
            var trainingDataView = mlContext.Data.LoadFromTextFile<RawLabeledEntry>(_trainDataPath, hasHeader: true);
            var testDataView = mlContext.Data.LoadFromTextFile<RawLabeledEntry>(_testDataPath, hasHeader: true);

            // STEP 2: Bible.Common data process configuration with pipeline data transformations          
            var dataProcessPipeline = mlContext.Transforms.Text.FeaturizeText(outputColumnName: DefaultColumnNames.Features, inputColumnName:nameof(RawLabeledEntry.Text));

            // (OPTIONAL) Peek data (such as 2 records) in training DataView after applying the ProcessPipeline's transformations into "Features" 
            ConsoleHelper.PeekDataViewInConsole(mlContext, trainingDataView, dataProcessPipeline, 2);
            ConsoleHelper.PeekVectorColumnDataInConsole(mlContext, DefaultColumnNames.Features, trainingDataView, dataProcessPipeline, 1);

            // STEP 3: Set the training algorithm, then create and config the modelBuilder                            
            var trainer = mlContext.BinaryClassification.Trainers.FastTree();
            var trainingPipeline = dataProcessPipeline.Append(trainer);

            // STEP 4: Train the model fitting to the DataSet
            Console.WriteLine("=============== Training the model ===============");
            var trainedModel = trainingPipeline.Fit(trainingDataView);

            // STEP 5: Evaluate the model and show accuracy stats
            EvaluateModel(mlContext, trainedModel, testDataView, trainer);
            
            // STEP 6: Save/persist the trained model to a .ZIP file
            return mlContext.SaveModel(_modelPath, trainedModel);
        }

        private static void EvaluateModel(MLContext mlContext, ITransformer trainedModel, IDataView testDataView,
            FastTreeBinaryClassificationTrainer trainer)
        {
            Console.WriteLine("===== Evaluating Model's accuracy with Test data =====");
            var predictions = trainedModel.Transform(testDataView);
            var metrics = mlContext.BinaryClassification.Evaluate(data: predictions, label: DefaultColumnNames.Label, score: DefaultColumnNames.Score);

            ConsoleHelper.PrintBinaryClassificationMetrics(trainer.ToString(), metrics);
        }
    }
}