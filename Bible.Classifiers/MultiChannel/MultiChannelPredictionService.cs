using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bible.Classifiers.Dtos;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace Bible.Classifiers.MultiChannel
{
    using Predictor = PredictionEngine<RawTwitterSentimentEntity, MultichannelPrediction>;

    public interface IMultiChannelPredictionService
    {
        IReadOnlyCollection<AugmentedMultichannelPrediction> Predict(string phrase);
    }

    public class MultiChannelPredictionService : IMultiChannelPredictionService
    {
        private readonly string _modelPath;
        private readonly MLContext _mlContext;
        private readonly Lazy<Predictor> _predictionEngine;

        public MultiChannelPredictionService(string modelPath)
        {
            _modelPath    = modelPath.ToAbsolutePath();
            _mlContext    = new MLContext(seed: 1);
            _predictionEngine = new Lazy<Predictor>(LoadPredictionEngine);
        }

        public IReadOnlyCollection<AugmentedMultichannelPrediction> Predict(string phrase)
        {
            var verse = new RawTwitterSentimentEntity {Content = phrase};
            var prediction = _predictionEngine.Value.Predict(verse);
            return GetThreeBestPredictions(prediction);
        }

        private IReadOnlyCollection<AugmentedMultichannelPrediction> GetThreeBestPredictions(MultichannelPrediction prediction)
        {
            var scores = prediction.Score;
            var size   = scores.Length;
            var (first, second, third) = GetIndexesOfTopThreeScores(scores, size);
            
            VBuffer<ReadOnlyMemory<char>> slotNames = default;
            _predictionEngine.Value.OutputSchema[nameof(MultichannelPrediction.Score)].GetSlotNames(ref slotNames);
            
            return new []
            {
                new AugmentedMultichannelPrediction(slotNames.GetItemOrDefault(first).ToString(), scores[first], first),
                new AugmentedMultichannelPrediction(slotNames.GetItemOrDefault(second).ToString(), scores[second], second),
                new AugmentedMultichannelPrediction(slotNames.GetItemOrDefault(third).ToString(), scores[third], third),
            };
        }
        
        private (int, int, int) GetIndexesOfTopThreeScores(IReadOnlyList<float> scores, int n)
        {
            int   i;
            float first, second;
            if (n < 3)
            {
                Console.WriteLine("Invalid Input");
                return(-1, -1, -1);
            }
            var third = first = second = 000;
            for (i = 0; i < n; i++)
            {
                // If current element is  
                // smaller than first 
                if (scores[i] > first)
                {
                    third  = second;
                    second = first;
                    first  = scores[i];
                }
                // If arr[i] is in between first 
                // and second then update second 
                else if (scores[i] > second)
                {
                    third  = second;
                    second = scores[i];
                }

                else if (scores[i] > third)
                    third = scores[i];
            }
            var scoresList = scores.ToList();
            return (scoresList.IndexOf(first),
                    scoresList.IndexOf(second),
                    scoresList.IndexOf(third));
        }

        private Predictor LoadPredictionEngine()
        {
            var trainedModel = LoadTrainedModel();
            return trainedModel
                .CreatePredictionEngine<RawTwitterSentimentEntity, 
                                        MultichannelPrediction>(_mlContext);
        }

        private ITransformer LoadTrainedModel()
        {
            using (var stream = new FileStream(_modelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return _mlContext.Model.Load(stream);
            }
        }
    }
}