using System;
using System.IO;
using Bible.Classifiers.Dtos;
using Bible.Common;
using Microsoft.Data.DataView;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;

namespace Bible.Classifiers.MultiChannel
{
    public enum MultiChannelTrainerStrategy : int { SdcaMultiClassTrainer = 1, OvaAveragedPerceptronTrainer = 2 };

    public interface IMultiChannelTrainerService
    {
        ITransformer BuildTrainEvaluateAndSaveModel();
    }

    public class MultiChannelTrainerService : IMultiChannelTrainerService
    {
        private readonly string _baseModelPath;
        private readonly MultiChannelTrainerStrategy _trainerStrategy;
        private static readonly string BaseDatasetsPath = @"./Data".ToAbsolutePath();
        private readonly string _trainDataPath;
        private readonly string _modelPath;
        private readonly MLContext _mlContext;

        public MultiChannelTrainerService(string trainDataPath, 
                                          string baseModelPath,
                                          MultiChannelTrainerStrategy trainerStrategy,
                                          string modelFileName = "MultichannelClassModel")
        {
            _trainDataPath   = trainDataPath;
            _baseModelPath   = baseModelPath;
            _trainerStrategy = trainerStrategy;
            _modelPath       = $"{baseModelPath.ToAbsolutePath()}/{modelFileName}.zip";
            _mlContext       = new MLContext(seed: 1);
        }
        
        public ITransformer BuildTrainEvaluateAndSaveModel()
        {
            return BuildTrainEvaluateAndSaveModel(_mlContext);
        }

        private IEstimator<ITransformer> GetTrainerForChosenStrategy(MLContext mlContext)
        {
            if (_trainerStrategy == MultiChannelTrainerStrategy.SdcaMultiClassTrainer)
            {
                return mlContext.MulticlassClassification.Trainers.StochasticDualCoordinateAscent();
            }
            var averagedPerceptronBinaryTrainer = mlContext.BinaryClassification.Trainers.AveragedPerceptron(DefaultColumnNames.Label, DefaultColumnNames.Features, numIterations: 10);
            // Compose an OVA (One-Versus-All) trainer with the BinaryTrainer.
            // In this strategy, a binary classification algorithm is used to train one classifier for each class, "
            // which distinguishes that class from all other classes. Prediction is then performed by running these binary classifiers, "
            // and choosing the prediction with the highest confidence score.
            return mlContext.MulticlassClassification.Trainers.OneVersusAll(averagedPerceptronBinaryTrainer);
        }
        
        private ITransformer BuildTrainEvaluateAndSaveModel(MLContext mlContext)
        {
            // STEP 1: Bible.Common data loading configuration
            var trainingDataView = mlContext.Data
                .LoadFromTextFile<RawTwitterSentimentEntity>(_trainDataPath, hasHeader: true);
            var dataProcessPipeline = ConfigureDataProcessPipeline(mlContext, trainingDataView);

            // STEP 3: Create the selected training algorithm/trainer
            var trainer = GetTrainerForChosenStrategy(mlContext);

            //Set the trainer/algorithm and map label to value (original readable state)
            var trainingPipeline = dataProcessPipeline
                .Append(trainer)
                .Append(mlContext.Transforms.Conversion.MapKeyToValue(DefaultColumnNames.PredictedLabel));

            var trainedModel = CrossValidateAndTrain(mlContext, trainingDataView, trainingPipeline, trainer);

            // STEP 6: Save/persist the trained model to a .ZIP file
            Console.WriteLine("=============== Saving the model to a file ===============");

            return mlContext.SaveModel(_modelPath, trainedModel);
        }

        private TransformerChain<KeyToValueMappingTransformer> CrossValidateAndTrain(MLContext mlContext,
                                                                                     IDataView trainingDataView,
                                                                                     EstimatorChain<KeyToValueMappingTransformer> trainingPipeline,
                                                                                     IEstimator<ITransformer> trainer)
        {
// STEP 4: Cross-Validate with single dataset (since we don't have two datasets, one for training and for evaluate)
            // in order to evaluate and get the model's accuracy metrics

            Console.WriteLine("=============== Cross-validating to get model's accuracy metrics ===============");

            //Measure cross-validation time
            var watchCrossValTime = System.Diagnostics.Stopwatch.StartNew();

            var crossValidationResults = mlContext
                .MulticlassClassification
                .CrossValidate(data: trainingDataView, estimator: trainingPipeline, numFolds: 6);

            //Stop measuring time
            watchCrossValTime.Stop();
            var elapsedMs = watchCrossValTime.ElapsedMilliseconds;
            Console.WriteLine($"Time Cross-Validating: {elapsedMs} miliSecs");

            ConsoleHelper.PrintMulticlassClassificationFoldsAverageMetrics(trainer.ToString(), crossValidationResults);

            return TrainModel(trainingDataView, trainingPipeline);
        }

        private static TransformerChain<KeyToValueMappingTransformer> TrainModel(IDataView trainingDataView, EstimatorChain<KeyToValueMappingTransformer> trainingPipeline)
        {
// STEP 5: Train the model fitting to the DataSet
            Console.WriteLine("=============== Training the model ===============");
            //Measure training time
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var trainedModel = trainingPipeline.Fit(trainingDataView);

            //Stop measuring time
            watch.Stop();
            var elapsedCrossValMs = watch.ElapsedMilliseconds;

            Console.WriteLine($"Time Training the model: {elapsedCrossValMs} miliSecs");
            return trainedModel;
        }

        private static EstimatorChain<ITransformer> ConfigureDataProcessPipeline(MLContext mlContext, IDataView trainingDataView)
        {
// STEP 2: Bible.Common data process configuration with pipeline data transformations
            var dataProcessPipeline = mlContext.Transforms.Conversion
                .MapValueToKey(outputColumnName: DefaultColumnNames.Label, inputColumnName: nameof(RawTwitterSentimentEntity.Sentiment))
                .Append(mlContext.Transforms.Text.FeaturizeText(DefaultColumnNames.Features, nameof(RawTwitterSentimentEntity.Content)))
                .AppendCacheCheckpoint(mlContext);
            // Use in-memory cache for small/medium datasets to lower training time. 
            // Do NOT use it (remove .AppendCacheCheckpoint()) when handling very large datasets.

            // (OPTIONAL) Peek data (such as 2 records) in training DataView after applying the ProcessPipeline's transformations into "Features" 
            ConsoleHelper.PeekDataViewInConsole(mlContext, trainingDataView, dataProcessPipeline, 2);
            return dataProcessPipeline;
        }
    }
}