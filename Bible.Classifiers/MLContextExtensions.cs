using System;
using Microsoft.ML;

namespace Bible.Classifiers
{
    public static class MLContextExtensions
    {
        public static ITransformer SaveModel(this MLContext me, string modelPath, ITransformer trainedModel)
        {
            using (var fileStream = modelPath.PathToNewFileStream())
            {
                me.Model.Save(trainedModel, fileStream);
            }

            Console.WriteLine("The model is saved to {0}", modelPath);

            return trainedModel;
        }
    }
}