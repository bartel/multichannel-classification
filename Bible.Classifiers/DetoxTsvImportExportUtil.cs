using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CsvHelper;
using CsvHelper.Configuration;

namespace Bible.Classifiers
{
    public class RawWikiDetoxEntry
    {
        private static Regex NoiseRegex = new Regex("(?:\\r?\\n|\\t+|\"+)", RegexOptions.Multiline);
        private static Regex SpacesRegex = new Regex("\\s+");
        
        public string id { get; set; }
        public string comment_text { get; set; }
        public int toxic { get; set; }
        public int severe_toxic { get; set; }
        public int obscene { get; set; }
        public int threat { get; set; }
        public int insult { get; set; }
        public int identity_hate { get; set; }

        public int SumAll() => toxic + severe_toxic + obscene + threat + insult + identity_hate > 0 ? 1 : 0;
        public string GetText() => new string[] {comment_text}
            .Select(x => NoiseRegex.Replace(x, " "))
            .Select(x => SpacesRegex.Replace(x, " "))
            .First();
    }
    
    public class DetoxTsvImportExportUtil
    {
        private readonly string _trainInPath;
        private readonly string _trainOutPath;
        private readonly string _testOutPath;
        private const int TrainTestThreshold = 80000;

        public DetoxTsvImportExportUtil(string trainInPath, string trainOutPath, string testOutPath)
        {
            _trainInPath = trainInPath;
            _trainOutPath = trainOutPath;
            _testOutPath = testOutPath;
        }

        public void ConvertAndSave()
        {
            using (var reader = new StreamReader(_trainInPath))
            using (var csv = new CsvReader(reader))
            {
                var records = csv.GetRecords<RawWikiDetoxEntry>()
                    .Select(x => new {Sentiment = x.SumAll(), SentimentText = x.GetText() })
                    .OrderBy(x => new Random().Next()).ToList();
                var trainRecords = records.Take(TrainTestThreshold);    
                var testRecords = records.Skip(TrainTestThreshold);
                
                var conf = new Configuration { Delimiter = "\t", IgnoreQuotes = false};
                WriteToTsv(conf, trainRecords, _trainOutPath);
                WriteToTsv(conf, testRecords, _testOutPath);
            }
        }

        private static void WriteToTsv(Configuration conf, 
                                       IEnumerable<object> trainRecords,
                                       string outPath)
        {
            using (var writer = new StreamWriter(outPath))
            using (var tsv = new CsvWriter(writer, conf))
            {
                tsv.WriteRecords(trainRecords);
            }
        }
    }
}